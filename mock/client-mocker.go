package main

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"path"
	"regexp"
	"strings"

	"gitlab.com/hathi-social/hathi-protocol"
)

// Usage: client-mocker -h <host> -p <port> <testfile...>
// -h          hostname to connect to. Default: localhost
// -p          port to connect to. Default: ???
// <testfile>  one or more files / directories containing test sequences

// Load the data contained in a list of files or directories, recursively.
func loadTests(directories []string) (testBlobs map[string]string) {
	// testBlob key is the filename, value is the data
	testBlobs = make(map[string]string, 0)
	// Used to track already seen files to prevent duplication or loops.
	processed := make(map[string]bool, 0)
	for i := 0; i < len(directories); i++ {
		current := directories[i]
		if e, ok := processed[current]; e == true && ok == true {
			continue // anti-recursion check
		}
		fd, err := os.Open(current)
		defer fd.Close()
		if err != nil {
			// Failed, skipping
			processed[current] = true
			continue
		}
		// Test current for file/dir
		fstat, err := fd.Stat()
		if err != nil {
			// Failed, skipping
			processed[current] = true
			continue
		}
		if fstat.IsDir() == true {
			// FIXME: directories aren't fully tested and known to work.
			// Get directory contents and append them to directories
			names, err := fd.Readdirnames(0)
			if err != nil {
				// TODO: Do what? We may still have names.
			}
			if names == nil {
				processed[current] = true
				continue // Nothing to add, continue
			}
			// Add the new names and trim off everything we have looked at
			// This way the list doesn't balloon unnecessarily
			directories = append(directories[i+1:], names...)
			i = 0 // reset for new list size
		} else {
			// Get file data and add to dataBlobs
			fileSize := fstat.Size()
			buf := make([]byte, fileSize)
			count, err := fd.Read(buf)
			if err != nil {
				processed[current] = true
				continue
			}
			if int64(count) != fileSize {
				// TODO: Read not the same as the file.... do what?
			}
			testBlobs[current] = string(buf)
		}
		// Success! Mark it.
		processed[current] = true
		fd.Close()
	}
	return testBlobs
}

// Represents a single send/recieve test pair, and their embeded variables
type TestPair struct {
	Name         string // Set to the file name, stripped of directories
	DataVars     map[string]*VarRef
	Data         *protocol.Packet
	ExpectedVars map[string]*VarRef
	Expected     *protocol.Packet
}

// Used to track a reference to a variable. *Not* the value itself
type VarRef struct {
	Name string // Name of the variable
	Set  bool   // Is the reference a set-variable command?
}

// Create variable reference from definition string. Usable as a detector.
func NewVarRef(name string) (v *VarRef) {
	// Variables in the tests are in the format {foo} or {foo=}
	// Trailing equal sign means it is a set command
	match, _ := regexp.MatchString(`{[[:alnum:]]+=?}`, name)
	if match {
		v = new(VarRef)
		v.Name = name[1 : len(name)-1] // Strip brackets
		last := len(v.Name) - 1
		if v.Name[last] == '=' {
			v.Name = v.Name[:last] // Strip trailing =
			v.Set = true
		} else {
			v.Set = false
		}
		return v
	} else {
		return nil // No match
	}
}

func extractVariables_value(value interface{}, key string) (varMap map[string]*VarRef, directReplace bool) {
	varMap = make(map[string]*VarRef, 0)
	directReplace = false // Tells the caller if it needs to insert a nulloid
	switch typedV := value.(type) {
	case map[string]interface{}:
		if key != "" {
			key += "^"
		}
		// recurse for each key
		for subKey, value := range typedV {
			subVars, dirRep := extractVariables_value(value, key+subKey)
			if dirRep { // replace variable with nulloid
				typedV[subKey] = 0
			}
			for k, v := range subVars { // Add sub-keys to map
				varMap[k] = v
			}
		}
	case []interface{}:
		// This will *only* work on interface{} lists. But that is all
		// that the JSON decoder should ever emit
		// recurse for each index
		if key != "" {
			key += "^"
		}
		for index, item := range typedV {
			subVars, dirRep := extractVariables_value(item,
				key+fmt.Sprintf("%d", index))
			if dirRep { // replace variable with nulloid
				typedV[index] = 0
			}
			for k, v := range subVars { // Add sub-keys to map
				varMap[k] = v
			}
		}
	case string:
		// check for varStr pattern
		varName := NewVarRef(typedV)
		if varName != nil {
			// save the Var
			varMap[key] = varName
			// Replace value with nulloid
			directReplace = true
		}
	}
	return varMap, directReplace
}

// Finds variables, stores them, and turns the references into valid null data
func extractVariables(packet map[string]interface{}) (varMap map[string]*VarRef) {
	// We know the root will never be a variable, we can ignore directReplace
	varMap, _ = extractVariables_value(packet, "")
	// Check varMap for packet fields
	// replace fields in map with nulloids that are valid for that type
	if _, ok := varMap["version"]; ok == true {
		packet["version"] = 1
	}
	if _, ok := varMap["request"]; ok == true {
		packet["request"] = 0
	}
	if _, ok := varMap["session"]; ok == true {
		packet["session"] = ""
	}
	if _, ok := varMap["function"]; ok == true {
		packet["function"] = ""
	}
	if _, ok := varMap["actor"]; ok == true {
		packet["actor"] = ""
	}
	if _, ok := varMap["object-id"]; ok == true {
		packet["object-id"] = ""
	}
	if _, ok := varMap["error"]; ok == true {
		packet["error"] = ""
	}
	if _, ok := varMap["username"]; ok == true {
		packet["username"] = ""
	}
	if _, ok := varMap["password"]; ok == true {
		packet["password"] = ""
	}
	return varMap
}

// Turn the contents of a text file into a test sequence.
func parseBlob(blob string) (sequence []*TestPair, err error) {
	sequence = make([]*TestPair, 0)
	// Divide the blob into individual test pairs
	reg := regexp.MustCompile("(\\n(\\s*)%(\\s*)\\n)")
	parts := reg.Split(blob, -1)
	for _, part := range parts {
		pair := new(TestPair)
		pair.Name = part // Filename
		read := strings.NewReader(part)
		decoder := json.NewDecoder(read)
		var datum interface{}
		testParts := make([]*protocol.Packet, 0)
		for i := 0; decoder.More(); i++ {
			if err := decoder.Decode(&datum); err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}
			// extract variables
			packet := datum.(map[string]interface{})
			if i == 0 {
				pair.DataVars = extractVariables(packet)
			} else {
				pair.ExpectedVars = extractVariables(packet)
			}
			// Parsing machinery demands that the request field exist
			packet["request"] = 0
			pkt, err := protocol.ParseMap(packet)
			if err != nil {
				return nil, err
			}
			testParts = append(testParts, pkt)
		}
		// get two objects: test data, expected result
		if len(testParts) != 2 {
			// Should be getting a test + expected result. Raise error
			return nil, errors.New("Incorrect number of objects in pair")
		}
		pair.Data = testParts[0]
		pair.Expected = testParts[1]
		sequence = append(sequence, pair)
	}
	return sequence, nil
}

// Insert values into designated variable locations in a list
func spliceVariables_list(target []interface{}, varMap map[string]*VarRef, variables map[string]interface{}, keyRoot string) {
	if keyRoot != "" {
		keyRoot += "^"
	}
	for i, v := range target {
		fullKey := keyRoot + fmt.Sprintf("%d", i)
		if vr, ok := varMap[fullKey]; ok == true {
			// Splice
			target[i] = variables[vr.Name]
		} else {
			// Check for recursion
			switch typedV := v.(type) {
			case map[string]interface{}:
				spliceVariables_map(typedV, varMap, variables, fullKey)
			case []interface{}:
				spliceVariables_list(typedV, varMap, variables, fullKey)
			}
		}
	}
}

// Insert values into designated variables locations in a map
func spliceVariables_map(target map[string]interface{}, varMap map[string]*VarRef, variables map[string]interface{}, keyRoot string) {
	if keyRoot != "" {
		keyRoot += "^"
	}
	for k, v := range target {
		fullKey := keyRoot + k
		if vn, ok := varMap[fullKey]; ok == true {
			// Splice
			target[k] = variables[vn.Name]
		} else {
			// Check for recursion
			switch typedV := v.(type) {
			case map[string]interface{}:
				spliceVariables_map(typedV, varMap, variables, fullKey)
			case []interface{}:
				spliceVariables_list(typedV, varMap, variables, fullKey)
			}
		}
	}
}

func getInt(v interface{}) (i int) {
	switch typedV := v.(type) {
	case int:
		return typedV
	case uint:
		return int(typedV)
	}
	return 0
}

// Injects variable values into the correct locations of a packet
func spliceVariables(pkt *protocol.Packet, varMap map[string]*VarRef, variables map[string]interface{}) {
	// Check packet fields
	if vn, ok := varMap["version"]; ok == true && vn.Set == false {
		pkt.Version = getInt(variables[vn.Name])
	}
	if vn, ok := varMap["request"]; ok == true && vn.Set == false {
		pkt.RequestID = getInt(variables[vn.Name])
	}
	if vn, ok := varMap["session"]; ok == true && vn.Set == false {
		pkt.SessionID = variables[vn.Name].([16]byte)
	}
	if vn, ok := varMap["function"]; ok == true && vn.Set == false {
		pkt.Function = variables[vn.Name].(string)
	}
	if vn, ok := varMap["actor"]; ok == true && vn.Set == false {
		pkt.Actor = variables[vn.Name].(string)
	}
	if vn, ok := varMap["object-id"]; ok == true && vn.Set == false {
		pkt.ObjectID = variables[vn.Name].(string)
	}
	if vn, ok := varMap["error"]; ok == true && vn.Set == false {
		pkt.Error = variables[vn.Name].(string)
	}
	if vn, ok := varMap["username"]; ok == true && vn.Set == false {
		pkt.Username = variables[vn.Name].(string)
	}
	if vn, ok := varMap["password"]; ok == true && vn.Set == false {
		pkt.Password = variables[vn.Name].(string)
	}
	// Recursive check on Object
	spliceVariables_map(pkt.Object, varMap, variables, "object")
	// Recursive check on ExtraFields
	spliceVariables_map(pkt.ExtraFields, varMap, variables, "extra-fields")
}

// Compares two lists and extracts values for variable set commands
func compareSetLists(expected []interface{}, got []interface{}, varMap map[string]*VarRef, variables map[string]interface{}, keyRoot string) (ok bool) {
	if keyRoot != "" {
		keyRoot += "^"
	}
	if len(expected) != len(got) {
		fmt.Println("List length mis-match", expected, got, keyRoot)
		return false
	}
	for key, _ := range expected {
		ok = compareSetValues(expected[key], got[key], varMap, variables,
			keyRoot+fmt.Sprintf("%d", key))
		if ok == false {
			return false
		}
	}
	return true
}

// Compares two maps and extracts values for variable set commands
func compareSetMaps(expected map[string]interface{}, got map[string]interface{}, varMap map[string]*VarRef, variables map[string]interface{}, keyRoot string) (ok bool) {
	if keyRoot != "" {
		keyRoot += "^"
	}
	if len(expected) != len(got) {
		fmt.Println("Map length mis-match", expected, got, keyRoot)
		return false
	}
	for key, _ := range expected {
		ok = compareSetValues(expected[key], got[key], varMap, variables,
			keyRoot+key)
		if ok == false {
			return false
		}
	}
	return true
}

// Compares two values and extracts values for variable set commands.
// Most of the brains of the comparison / set system are here.
func compareSetValues(expected interface{}, got interface{}, varMap map[string]*VarRef, variables map[string]interface{}, key string) (equal bool) {
	switch typedExp := expected.(type) {
	case map[string]interface{}:
		switch typedGot := got.(type) {
		case map[string]interface{}:
			return compareSetMaps(typedExp, typedGot, varMap, variables, key)
		default:
			fmt.Println("Type mis-match with map", expected, got, key)
			return false // Type mis-match
		}
	case []interface{}:
		switch typedGot := got.(type) {
		case []interface{}:
			return compareSetLists(typedExp, typedGot, varMap, variables, key)
		default:
			fmt.Println("Type mis-match, with list", expected, got, key)
			return false // Type mis-match
		}
	default:
		switch got.(type) {
		case map[string]interface{}:
			fmt.Println("Type mis-match with value", expected, got, key)
			return false // Type mis-match
		case []interface{}:
			fmt.Println("Type mis-match with value", expected, got, key)
			return false // Type mis-match
		default:
			// Both are simple values
			if vr, ok := varMap[key]; ok == true && vr.Set == true {
				// This location has a variable set command.
				// We don't compare, we simply store the data
				// so later tests may  use it.
				variables[vr.Name] = got
				return true
			} else {
				// Normal comparison. Any variables were already spliced in.
				equal = expected == got
				if equal == false {
					fmt.Println("Value unequal", expected, got, key)
				}
				return equal
			}
		}
	}
}

// Run the comparison / set routines on a packet
func checkPacket(expected *protocol.Packet, got *protocol.Packet, varMap map[string]*VarRef, variables map[string]interface{}) (err error) {
	// Version
	if compareSetValues(expected.Version, got.Version, varMap, variables, "version") == false {
		return errors.New(fmt.Sprintf("Mismatched version: %d, %d",
			expected.Version, got.Version))
	}
	// Session
	if compareSetValues(expected.SessionID, got.SessionID, varMap, variables, "session") == false {
		return errors.New(fmt.Sprintf("Mismatched session ID: %v, %v",
			expected.SessionID, got.SessionID))
	}
	// Function
	if compareSetValues(expected.Function, got.Function, varMap, variables, "function") == false {
		return errors.New(fmt.Sprintf(`Mismatched function: "%s", "%s"`,
			expected.Function, got.Function))
	}
	// Actor
	if compareSetValues(expected.Actor, got.Actor, varMap, variables, "actor") == false {
		return errors.New(fmt.Sprintf(`Mismatched actor: "%s", "%s"`,
			expected.Actor, got.Actor))
	}
	// ObjectID
	if compareSetValues(expected.ObjectID, got.ObjectID, varMap, variables, "object-id") == false {
		return errors.New(fmt.Sprintf(`Mismatched object ID: "%s", "%s"`,
			expected.ObjectID, got.ObjectID))
	}
	// Error
	if compareSetValues(expected.Error, got.Error, varMap, variables, "error") == false {
		return errors.New(fmt.Sprintf(`Mismatched error: "%s", "%s"`,
			expected.Error, got.Error))
	}
	// Username
	if compareSetValues(expected.Username, got.Username, varMap, variables, "username") == false {
		return errors.New(fmt.Sprintf(`Mismatched username: "%s", "%s"`,
			expected.Username, got.Username))
	}
	// Password
	if compareSetValues(expected.Password, got.Password, varMap, variables, "password") == false {
		return errors.New(fmt.Sprintf(`Mismatched password: "%s", "%s"`,
			expected.Password, got.Password))
	}
	// Object
	if compareSetMaps(expected.Object, got.Object, varMap, variables, "object") == false {
		return errors.New(fmt.Sprintf("Mismatched object: %v, %v",
			expected.Object, got.Object))
	}
	// ExtraFields
	if compareSetMaps(expected.ExtraFields, got.ExtraFields, varMap, variables, "") == false {
		return errors.New(fmt.Sprintf("Mismatched extra fields: %v, %v",
			expected.ExtraFields, got.ExtraFields))
	}
	return nil
}

func main() {
	// Argument handling
	var hostname string
	var portnum int
	var logLevel uint
	flag.StringVar(&hostname, "h", "localhost", "Hostname to connect to")
	flag.IntVar(&portnum, "p", 8080, "Port number to connect to")
	flag.UintVar(&logLevel, "l", 0, "Log level")
	flag.Parse()
	directories := flag.Args() // returns a []string
	protocol.SetLogLevel(logLevel)
	// Load test data
	testBlobs := loadTests(directories)
	sequences := make(map[string][]*TestPair, 0)
	for name, blob := range testBlobs {
		seq, err := parseBlob(blob)
		if err != nil {
			fmt.Println("ERROR parsing file:", name, err)
			continue
		}
		sequences[name] = seq
	}
	if len(sequences) == 0 {
		fmt.Println("No valid test sequences provided, exiting")
		return
	}
	// dial connection
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", hostname, portnum))
	if err != nil { // Failed to connect
		fmt.Println(err)
		return
	}
	// ==========================
	// Have data, begin the tests
	// start protocol
	endpoint := protocol.NewEndpointKernel(conn)
	endpoint.Run()
	defer endpoint.Stop()
	defer conn.Close() // We want this to happen *after* the endpoint stops
	// Run the sequences
	// client-mocker manages the request IDs because the test writer has no
	// possible way to know what they would be, or in what order the tests
	// will be run.
	var requestID int
	requestID = 1
	for seqName, sequence := range sequences {
		fmt.Println("Running sequence:", seqName)
		seqLen := len(sequence)
		fileName := path.Base(seqName)
		variables := make(map[string]interface{}, 0)
		for pairNum, pair := range sequence {
			fmt.Printf("%s... %d/%d\n", fileName, pairNum+1, seqLen)
			// Fill in the packet details that client-mocker manages itself
			pair.Data.RequestID = requestID
			spliceVariables(pair.Data, pair.DataVars, variables)
			// Send and wait...
			endpoint.SendPacket(pair.Data)
			pkt := endpoint.WaitForPacket()
			if pkt == nil {
				if endpoint.IsClosed() {
					fmt.Println("ERROR; connection closed")
				} else {
					fmt.Println("ERROR, no response")
				}
			}
			// Splice variables into the expected packet
			spliceVariables(pair.Expected, pair.ExpectedVars, variables)
			// Check recieved packet against expectations
			if pkt.RequestID != requestID {
				// FAIL
				fmt.Println("ERROR, incorrect requestID:", pkt.RequestID)
				return
			}
			if result := checkPacket(pair.Expected, pkt, pair.ExpectedVars, variables); result != nil {
				// FAIL
				fmt.Println("ERROR, packet doesn't match:", result.Error())
				return
			}
			requestID++
		}
	}
	fmt.Println("All sequences successful.")
	endpoint.Stop()
	conn.Close()
}
