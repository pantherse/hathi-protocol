package main

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-protocol"
	"testing"
	//"fmt"
)

func Test_NewVarName(t *testing.T) {
	// Test no match
	vn := NewVarRef("blah")
	if vn != nil {
		t.Fatal("NewVarName failure:", vn)
	}
	// Test match
	vn = NewVarRef("{foo}")
	if vn.Name != "foo" || vn.Set != false {
		t.Fatal("NewVarName failure:", vn)
	}
	// Test set match
	vn = NewVarRef("{foo=}")
	if vn.Name != "foo" || vn.Set != true {
		t.Fatal("NewVarName failure:", vn)
	}
}

func Test_extractVariables_value(t *testing.T) {
	// Test
	testPkt := map[string]interface{}{"foo": 3, "bar": "{ONE}",
		"baz": "{TWO=}", "qwerty": "str-val",
		"obj":  map[string]interface{}{"asdf": 42, "m": "{MAP}"},
		"list": []interface{}{3, "{T}"}}
	varMap, directReplace := extractVariables_value(testPkt, "")
	// Check
	if len(varMap) != 4 {
		t.Fatal("extractVariables_value varMap fail:", varMap)
	} else if directReplace != false {
		t.Fatal("extractVariables_value directReplace error")
	} else if varMap["bar"].Name != "ONE" || varMap["bar"].Set != false {
		t.Fatal("extractVariables_value varMap fail:", varMap)
	} else if varMap["baz"].Name != "TWO" || varMap["baz"].Set != true {
		t.Fatal("extractVariables_value varMap fail:", varMap)
	} else if varMap["list^1"].Name != "T" || varMap["list^1"].Set != false {
		t.Fatal("extractVariables_value varMap fail:", varMap)
	} else if varMap["obj^m"].Name != "MAP" || varMap["obj^m"].Set != false {
		t.Fatal("extractVariables_value varMap fail:", varMap)
	}
}

func Test_extractVariables(t *testing.T) {
	// Test
	testPkt := map[string]interface{}{"version": "{1}", "request": "{2}",
		"session": "{4}", "function": "{5}", "actor": "{6}",
		"object-id": "{7}", "error": "{8}", "username": "{9}",
		"password": "{10}"}
	varMap := extractVariables(testPkt)
	// Check
	nulloidFail := false
	if len(varMap) != 9 {
		t.Fatal("extractVariables fail:", varMap)
	} else if testPkt["version"] != 1 {
		nulloidFail = true
	} else if testPkt["request"] != 0 {
		nulloidFail = true
	} else if testPkt["session"] != "" {
		nulloidFail = true
	} else if testPkt["function"] != "" {
		nulloidFail = true
	} else if testPkt["actor"] != "" {
		nulloidFail = true
	} else if testPkt["object-id"] != "" {
		nulloidFail = true
	} else if testPkt["error"] != "" {
		nulloidFail = true
	} else if testPkt["username"] != "" {
		nulloidFail = true
	} else if testPkt["password"] != "" {
		nulloidFail = true
	}
	if nulloidFail {
		t.Fatal("extractVariables fail:", testPkt)
	}
}

func Test_spliceVariables_list(t *testing.T) {
	// Prepare input
	targetList := []interface{}{"foo", 0, []interface{}{"bar", 0},
		map[string]interface{}{"baz": 0}}
	varMap := map[string]*VarRef{
		"1":     &VarRef{Name: "a", Set: false},
		"2^1":   &VarRef{Name: "b", Set: false},
		"3^baz": &VarRef{Name: "c", Set: false}}
	variables := map[string]interface{}{"a": 1, "b": 2, "c": 3}
	// Test
	spliceVariables_list(targetList, varMap, variables, "")
	// Check
	spliceFail := false
	if targetList[0] != "foo" {
		spliceFail = true
	} else if targetList[1] != 1 {
		spliceFail = true
	} else if targetList[2].([]interface{})[0] != "bar" {
		spliceFail = true
	} else if targetList[2].([]interface{})[1] != 2 {
		spliceFail = true
	} else if targetList[3].(map[string]interface{})["baz"] != 3 {
		spliceFail = true
	}
	if spliceFail {
		t.Fatal("spliceVariables_list failure:", targetList)
	}
}

func Test_spliceVariables_map(t *testing.T) {
	// Prepare input
	targetMap := map[string]interface{}{"foo": 0, "bar": "quux",
		"baz": map[string]interface{}{"one": 0, "two": 100}}
	varMap := map[string]*VarRef{
		"foo":     &VarRef{Name: "A", Set: false},
		"baz^one": &VarRef{Name: "B", Set: false}}
	variables := map[string]interface{}{"A": 42, "B": 23}
	// Test
	spliceVariables_map(targetMap, varMap, variables, "")
	// Check
	spliceFail := false
	if targetMap["foo"] != 42 {
		spliceFail = true
	} else if targetMap["bar"] != "quux" {
		spliceFail = true
	} else if targetMap["baz"].(map[string]interface{})["one"] != 23 {
		spliceFail = true
	} else if targetMap["baz"].(map[string]interface{})["two"] != 100 {
		spliceFail = true
	}
	if spliceFail {
		t.Fatal("spliceVariables_map failure:", targetMap)
	}
}

func Test_spliceVariables(t *testing.T) {
	// Prepare input
	pkt := protocol.Packet{Version: 1, RequestID: 0,
		SessionID: [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		Function:  "", Actor: "", ObjectID: "", Error: "", Username: "",
		Password: ""}
	varMap := map[string]*VarRef{
		"version":   &VarRef{Name: "A", Set: false},
		"request":   &VarRef{Name: "B", Set: false},
		"session":   &VarRef{Name: "D", Set: false},
		"function":  &VarRef{Name: "E", Set: false},
		"actor":     &VarRef{Name: "F", Set: false},
		"object-id": &VarRef{Name: "G", Set: false},
		"error":     &VarRef{Name: "H", Set: false},
		"username":  &VarRef{Name: "I", Set: false},
		"password":  &VarRef{Name: "J", Set: false}}
	variables := map[string]interface{}{"A": 42, "B": 23,
		"D": [16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		"E": "foo", "F": "bar", "G": "/cafe", "H": "none", "I": "luser",
		"J": "pw123"}
	// Test
	spliceVariables(&pkt, varMap, variables)
	// Check
	// TODO: session ID check
	spliceFail := false
	if pkt.Version != 42 {
		spliceFail = true
	} else if pkt.RequestID != 23 {
		spliceFail = true
	} else if pkt.SessionID != [16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
		12, 13, 14, 15} {
		spliceFail = true
	} else if pkt.Function != "foo" {
		spliceFail = true
	} else if pkt.Actor != "bar" {
		spliceFail = true
	} else if pkt.ObjectID != "/cafe" {
		spliceFail = true
	} else if pkt.Error != "none" {
		spliceFail = true
	} else if pkt.Username != "luser" {
		spliceFail = true
	} else if pkt.Password != "pw123" {
		spliceFail = true
	}
	if spliceFail {
		t.Fatal("spliceVariables failure:", pkt)
	}
}

func Test_compareSetLists(t *testing.T) {
	varMap := map[string]*VarRef{
		"1^0": &VarRef{Name: "a", Set: false},
		"1^1": &VarRef{Name: "b", Set: true}}
	variables := map[string]interface{}{"a": 10, "b": nil}
	// Test flat equal
	flatExp := []interface{}{"foo", 3}
	flatGot := []interface{}{"foo", 3}
	ok := compareSetLists(flatExp, flatGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetLists flat equal fail")
	}
	// Test flat un-equal
	flatGot = []interface{}{"foo", 3}
	ok = compareSetLists(flatExp, flatGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetLists flat equal fail")
	}
	// Test recursive equal
	recursiveExp := []interface{}{"foo", 3, []interface{}{1}}
	recursiveGot := []interface{}{"foo", 3, []interface{}{1}}
	ok = compareSetLists(recursiveExp, recursiveGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetLists recursive equal fail")
	}
	// Test recursive un-equal
	recursiveGot = []interface{}{"foo", 3, []interface{}{1, 2}}
	ok = compareSetLists(recursiveExp, recursiveGot, varMap, variables, "")
	if ok != false {
		t.Fatal("compareSetLists recursive un-equal fail")
	}
	// Test recursive set/no-set
	setExp := []interface{}{"foo", []interface{}{10, 0}}
	setGot := []interface{}{"foo", []interface{}{10, 42}}
	ok = compareSetLists(setExp, setGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetLists recursive set/no-set ok fail")
	} else if variables["a"] != 10 || variables["b"] != 42 {
		t.Fatal("compareSetLists recursive set/no-set fail", variables)
	}
}

func Test_compareSetMaps(t *testing.T) {
	varMap := map[string]*VarRef{
		"bar^quux": &VarRef{Name: "a", Set: false},
		"bar^asdf": &VarRef{Name: "b", Set: true}}
	variables := map[string]interface{}{"a": 10, "b": nil}
	// Test flat equal
	flatExp := map[string]interface{}{"foo": 23, "bar": 42}
	flatGot := map[string]interface{}{"foo": 23, "bar": 42}
	ok := compareSetMaps(flatExp, flatGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetMaps flat equal fail")
	}
	// Test flat un-equal
	flatGot = map[string]interface{}{"foo": 23, "bar": 65, "boo": 99}
	ok = compareSetMaps(flatExp, flatGot, varMap, variables, "")
	if ok != false {
		t.Fatal("compareSetMaps flat un-equal fail")
	}
	// Test recursive equal
	recursiveExp := map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"baz": 1}}
	recursiveGot := map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"baz": 1}}
	ok = compareSetMaps(recursiveExp, recursiveGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetMaps recursive equal fail")
	}
	// Test recursive un-equal
	recursiveExp = map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"baz": 1}}
	recursiveGot = map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"baz": 2}}
	ok = compareSetMaps(recursiveExp, recursiveGot, varMap, variables, "")
	if ok != false {
		t.Fatal("compareSetMaps recursive un-equal fail")
	}
	// Test recursive set/no-set
	setExp := map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"quux": 1, "asdf": 0}}
	setGot := map[string]interface{}{"foo": 23,
		"bar": map[string]interface{}{"quux": 1, "asdf": 42}}
	ok = compareSetMaps(setExp, setGot, varMap, variables, "")
	if ok != true {
		t.Fatal("compareSetMaps recursive set/no-set ok fail")
	} else if variables["a"] != 10 || variables["b"] != 42 {
		t.Fatal("compareSetMaps recursive set/no-set fail", variables)
	}
}

func Test_compareSetValues(t *testing.T) {
	emptyVarMap := map[string]*VarRef{}
	emptyVariables := map[string]interface{}{}
	// Test basic equal compare
	equal := compareSetValues(2, 2, emptyVarMap, emptyVariables, "")
	if equal != true {
		t.Fatal("compareSetValues fail 2 == 2")
	}
	equal = compareSetValues("foo", "foo", emptyVarMap, emptyVariables, "")
	if equal != true {
		t.Fatal(`compareSetValues fail "foo" == "foo"`)
	}
	// Test basic un-equal compare
	equal = compareSetValues(2, 3, emptyVarMap, emptyVariables, "")
	if equal != false {
		t.Fatal("compareSetValues fail 2 != 3")
	}
	equal = compareSetValues("foo", "bar", emptyVarMap, emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail "foo" != "bar"`)
	}
	equal = compareSetValues("foo", 2, emptyVarMap, emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail "foo" != 2`)
	}
	// Test value vs map type mis-match
	equal = compareSetValues("foo", map[string]interface{}{}, emptyVarMap,
		emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail "foo" != map`)
	}
	// Test value vs list type mis-match
	equal = compareSetValues("foo", []interface{}{}, emptyVarMap,
		emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail "foo" != list`)
	}
	// Test variable set
	varMap := map[string]*VarRef{"foo": &VarRef{Name: "a", Set: true}}
	variables := map[string]interface{}{"a": nil}
	equal = compareSetValues(0, 42, varMap, variables, "foo")
	if equal != true || variables["a"] != 42 {
		t.Fatal("compareSetVariables fail set 'foo' -> 'a' to 42:", variables)
	}
	// Test variable no set
	varMap = map[string]*VarRef{"foo": &VarRef{Name: "a", Set: false}}
	variables = map[string]interface{}{"a": nil}
	equal = compareSetValues(42, 42, varMap, variables, "foo")
	if equal != true || variables["a"] != nil {
		t.Fatal("compareSetVariables fail no-set:", variables)
	}
	// Test map recurse
	equal = compareSetValues(
		map[string]interface{}{"foo": 23},
		map[string]interface{}{"foo": 23},
		emptyVarMap, emptyVariables, "")
	if equal != true {
		t.Fatal(`compareSetValues fail map recurse`)
	}
	// Test map mis-match
	equal = compareSetValues(map[string]interface{}{"foo": 23}, 42,
		emptyVarMap, emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail map type mis-match`)
	}
	// Test list recurse
	equal = compareSetValues(
		[]interface{}{42}, []interface{}{42},
		emptyVarMap, emptyVariables, "")
	if equal != true {
		t.Fatal(`compareSetValues fail list recurse`)
	}
	// Test list mis-match
	equal = compareSetValues([]interface{}{42}, 42,
		emptyVarMap, emptyVariables, "")
	if equal != false {
		t.Fatal(`compareSetValues fail list type mis-match`)
	}
}
