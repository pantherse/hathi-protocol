# Understanding the Hathi protocol

The Hathi protocol is a JSON protocol used for client/server, server/server, and intra-server-module communications.


## Design

Each possible field within the protocol has a specific and unambiguous meaning.
This does not mean that some fields won't be used slightly differently in
different contexts (such as 'session' also being used to return a session ID
on login), but that they will always contain the same type of data for the
same general purpose.

Most fields will default to some sort of nil value if not present in the
packet. Only a few fields ('version', "function", and 'request') are required
in a valid packet. Each packet is treated as an asyncronous remote procedure
call, with the response identified by having the same request number as the
original.

Packets are exchanged in request/response pairs. The request packet will have
the type of request in its 'function' field, and the response will have a
function of 'result', with the same request ID. The exceptions to this rule
are the ping/pong pair, or if a request calls a function that the server does
not support. In that case the response will be a 'not-implemented' instead of
a 'result'.


## Examples

### Request for a publicly visible object
```
{
  "version":1,
  "request":4,
  "function":"get-object",
  "object-id":"foo.com/joe/9627"
}
```

### Request for a private object
```
{
  "version":1,
  "request":4,
  "function":"get-object",
  "object-id":"foo.com/joe/9627"
  "actor":"foo.com/tom",
  "session":"0f1e2d3c4b5a69788796a5b4c3d2e1f0"
}
```

## Fields


### version

Required field. Unsigned Integer. This is the version of the protocol being
used. Currently always set to 1.


### request

Required field. Signed Integer. Used by the sender to identify the response
to a given packet. The request ID is specific to a single connection.

Normal packets should always use a positive integer. Negative integers are
reserved by the protocol for ping/pong exchanges. A request ID of 0 is used
for error responses that cannot be linked to a packet, such as when a packet
does not have a request field.


### function

Required field. String. Indicates the purpose of the packet.


### object

JSON object. Used for functions which take a JSON object.


### session

16 hex encoded bytes as string. The session ID used to identify a login. A
value of all zeros is treated the same as the field not being present: a
public request.


### actor

The actor that this request is being executed as.

### password

String. Used for login and new account creation


### error

String. Used in result packets when an error occurs. Result packets *must* be
checked for this field, and if it exists handle it accordingly.


### object-id

ObjectID string. The object being requested.


## Functions


### ping/pong

WIP: I have not yet decided for certain in optional pings is a good idea. The
intention is to disable them on one-shots.

A 'ping' is sent when no traffic has occured in a while, if the send pings
setting has been activated. A recieved ping is always responded to, even if
sending pings is disabled.

The correct response to a 'ping' is a 'pong' with the same request ID. The
reason for not responding with a standard 'result' packet is to make it easy
for the handling code to seperate the ping sequence from the rest of the data,
as well as semantic clarity.


### not-implemented

The not implemented message is sent in lieu of a result when a node recieves a
reuqest that it does not support. Standard request ID rules apply.


### result

Sent with the results or errors of a previous request. The request ID must be
the same as the packet being responded to. This is how the requestor
identifies which packet is being responded to.


### get-object

Requires: object-id

Optional: session, actor

Requests an object. If the object is publically accessable the session and
actor fields are unnecessary. Otherwise the actor must be an actor which is
allowed to see the object, and the session must be valid for that actor.
Returns a result with the object in the 'object' field if successful.


### post-outbox

Requires: session, actor, object

Posts an object to the given actor's outbox. The session must be valid for the
given actor.


### new-actor

Requires: actor, password

Creates a new credential in the instance. Returns an empty result on success.


### delete-actor

Requires: session

Delete an actor from the instance.

WIP: What to do with the actor's messages

### login

Requires: actor, password

Authentication request. If successful returns a session ID, if unsuccessful
returns an error.

### check-session

Requires: session

Verify if a session is valid, and associated to the given actor. If successful returns the session and actor; otherwise returns the text "invalid" in the error field.

### logout

Requires: session

Invalidates the current session.



##tricks & traps

##credits

##revision
